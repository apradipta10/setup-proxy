#!/bin/bash

# step x: update repo ubuntu
echo "Update ubuntu repo.."
sudo apt update

# Step 0: Clone Git Repository
echo "Cloning Git repository..."
cd
git clone https://github.com/proxidize/proxidize-android.git

# Step 1: Masuk ke Direktori yang Benar
echo "Masuk ke direktori proxidize-android..."
cd proxidize-android

# Step 2: Berikan Izin Eksekusi untuk Folder Server dan Isinya
echo "Mengatur izin eksekusi untuk folder server dan isinya..."
chmod +x server
chmod +x server/*
cd ..

# Step 3: Aktifkan Firewall
echo "Mengaktifkan firewall..."
sudo ufw enable

# Step 4: Buka Port yang Diperlukan
echo "Membuka port 2000 TCP/UDP dan 10000-60000 TCP/UDP..."
sudo ufw allow 2000/tcp
sudo ufw allow 2000/udp
sudo ufw allow 10000:60000/tcp
sudo ufw allow 10000:60000/udp
sudo ufw allow 22/tcp
sudo ufw allow 22/udp
sudo ufw reload

# Step 5: Set Proxydize Service
echo "Membuat unit file untuk Proxydize Server..."

SERVICE_FILE="/etc/systemd/system/proxydize.service"

sudo bash -c "cat > $SERVICE_FILE" <<EOL
[Unit]
Description=Proxidize Server
After=network.target

[Service]
Type=simple
ExecStart=/root/proxidize-android/server/server -c /root/proxidize-android/server/server.ini
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
EOL

# Reload daemon dan mulai layanan
echo "Reloading systemd daemon dan memulai layanan Proxydize..."
sudo systemctl daemon-reload
sudo systemctl enable proxydize
sudo systemctl start proxydize

# Memeriksa status layanan
echo "Memeriksa status layanan Proxydize..."
sudo systemctl status proxydize

# Step 6: Tambahkan Cron Job untuk Merestart Layanan Setiap 5 Menit
echo "Menambahkan cron job untuk merestart layanan setiap 5 menit..."
(crontab -l 2>/dev/null; echo "*/5 * * * * /usr/bin/systemctl restart proxydize") | sudo crontab -

echo "Setup selesai."
